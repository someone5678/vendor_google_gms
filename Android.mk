ifneq ($(WITH_GMS),false)

include $(call all-subdir-makefiles)

GMS_COMMON_DIR := vendor/google/gms/common/proprietary
PARTED_FILES_TO_MERGE_GMS := \
    $(GMS_COMMON_DIR)/product/priv-app/DevicePersonalizationPrebuiltPixel/DevicePersonalizationPrebuiltPixel.apk \
    $(GMS_COMMON_DIR)/product/priv-app/GoogleCamera/GoogleCamera.apk \
    $(GMS_COMMON_DIR)/product/priv-app/PrebuiltBugle/PrebuiltBugle.apk \
    $(GMS_COMMON_DIR)/product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreVic.apk \
    $(GMS_COMMON_DIR)/product/priv-app/Velvet/Velvet.apk \
    $(GMS_COMMON_DIR)/product/app/Photos/Photos.apk \
    $(GMS_COMMON_DIR)/product/app/PrebuiltGmail/PrebuiltGmail.apk \
    $(GMS_COMMON_DIR)/product/priv-app/PixelLiveWallpaperPrebuilt/PixelLiveWallpaperPrebuilt.apk

define merge-parted-files
	$(shell rm $(1))
	$(shell cat $(1).part* > $(1))
endef

$(foreach files,$(PARTED_FILES_TO_MERGE_GMS), \
	$(call merge-parted-files,$(files)) \
)

endif #WITH_GMS
