#
# Copyright (C) 2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifneq ($(WITH_GMS),false)

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCore

# GMS
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    GoogleContacts \
    GoogleDialer \
    LatinIMEGooglePrebuilt \
    NexusLauncherRelease \
    Phonesky \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SettingsIntelligenceGooglePrebuilt \
    TurboPrebuilt \
    Velvet

# Artifact path requirement allowlist
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/app/GoogleExtShared/GoogleExtShared.apk \
    system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk \
    system/etc/permissions/privapp-permissions-google.xml \
    system/priv-app/DocumentsUIGoogle/DocumentsUIGoogle.apk \
    system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk \
    system/priv-app/TagGoogle/TagGoogle.apk

# Recorder
TARGET_SUPPORTS_GOOGLE_RECORDER ?= true
ifeq ($(TARGET_SUPPORTS_GOOGLE_RECORDER),true)
PRODUCT_PACKAGES += \
    RecorderPrebuilt
endif

# arcore
TARGET_INCLUDE_STOCK_ARCORE ?= true
ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

# Live Wallpapers
TARGET_INCLUDE_LIVE_WALLPAPERS ?= true
ifeq ($(TARGET_INCLUDE_LIVE_WALLPAPERS),true)
PRODUCT_PACKAGES += \
    PixelLiveWallpaperPrebuilt
endif

# Satellite SOS
ifeq ($(TARGET_SUPPORTS_SATELLITE_SOS),true)
PRODUCT_PACKAGES += \
    SatelliteGatewayPrebuilt

PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/satellite_sos.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/satellite_sos.xml
endif

# FIR proximity sensors
ifeq ($(TARGET_SUPPORTS_FIR_PROXIMITY_SENSORS),true)
PRODUCT_PACKAGES += \
    HealthIntelligencePrebuilt

PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/firproximity.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/firproximity.xml
endif

ifeq ($(TARGET_SUPPORTS_PIXEL_SCREENSHOTS),true)
PRODUCT_PACKAGES += \
    PixelAIPrebuilt
    PearlOverlay2024
endif

ifeq ($(TARGET_SUPPORTS_PIXEL_STUDIO),true)
PRODUCT_PACKAGES += \
    CreativeAssistant
endif

# Call recording on Google Dialer
TARGET_SUPPORTS_CALL_RECORDING ?= true
ifeq ($(TARGET_SUPPORTS_CALL_RECORDING),true)
PRODUCT_PACKAGES += \
    com.google.android.apps.dialer.call_recording_audio.features.xml

PRODUCT_PACKAGES += \
    GoogleDialerConfOverlay
endif

# Charger Resources
ifneq ($(TARGET_CHARGER_RESOURCE_COPY_OUT),)
CHARGER_RESOURCE_COPY_OUT := $(TARGET_CHARGER_RESOURCE_COPY_OUT)
else
CHARGER_RESOURCE_COPY_OUT := $(TARGET_COPY_OUT_PRODUCT)
endif

PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/res/images/charger/battery_fail.png:$(CHARGER_RESOURCE_COPY_OUT)/etc/res/images/charger/battery_fail.png \
    vendor/google/gms/common/proprietary/product/etc/res/images/charger/battery_scale.png:$(CHARGER_RESOURCE_COPY_OUT)/etc/res/images/charger/battery_scale.png \
    vendor/google/gms/common/proprietary/product/etc/res/images/charger/main_font.png:$(CHARGER_RESOURCE_COPY_OUT)/etc/res/images/charger/main_font.png \
    vendor/google/gms/common/proprietary/product/etc/res/values/charger/animation.txt:$(CHARGER_RESOURCE_COPY_OUT)/etc/res/values/charger/animation.txt

# Bootanimation
ifneq ($(TARGET_BOOT_ANIMATION_COPY_OUT),)
BOOT_ANIMATION_COPY_OUT := $(TARGET_BOOT_ANIMATION_COPY_OUT)
else
BOOT_ANIMATION_COPY_OUT := $(TARGET_COPY_OUT_PRODUCT)
endif

PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/media/bootanimation-dark.zip:$(BOOT_ANIMATION_COPY_OUT)/media/bootanimation-dark.zip \
    vendor/google/gms/common/proprietary/product/media/bootanimation.zip:$(BOOT_ANIMATION_COPY_OUT)/media/bootanimation.zip

# Dex preopt
PRODUCT_DEXPREOPT_SPEED_APPS += \
    NexusLauncherRelease

# Dex preopt profiles
PRODUCT_DEX_PREOPT_PROFILE_DIR := vendor/google/dexopt_profiles/common

ifeq ($(TARGET_SUPPORTS_GOOGLE_BATTERY),true)
PRODUCT_PACKAGES += \
    TurboAdapter \
    libpowerstatshaldataprovider \
    libpowerstatshaldataprovider_libpowerstatshaldataprovider_symlink64
else ifeq ($(TARGET_SUPPORTS_GOOGLE_BATTERY),false)
# Include TurboAdapter without Google Battery support
PRODUCT_PACKAGES += \
    TurboAdapter_NoBatt \
    libpowerstatshaldataprovider
endif #TARGET_SUPPORTS_GOOGLE_BATTERY

ifeq ($(TARGET_SUPPORTS_ADPATIVE_CHARGING),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/adaptivecharging.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/adaptivecharging.xml
endif

ifeq ($(TARGET_SUPPORTS_GOOGLE_CAMERA),true)
PRODUCT_PACKAGES += \
    GoogleCamera
endif

ifeq ($(TARGET_GOOGLE_CAMERA_LARGE_RAM),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/GoogleCamera_6gb_or_more_ram.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/GoogleCamera_6gb_or_more_ram.xml
endif

ifeq ($(TARGET_SUPPORTS_DREAMLINER),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/dreamliner.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/dreamliner.xml \
    vendor/google/gms/common/proprietary/product/etc/permissions/com.google.android.apps.dreamliner.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.apps.dreamliner.xml

PRODUCT_PACKAGES += \
    DreamlinerPrebuilt \
    DreamlinerUpdater
endif

# Ship Tensor Pixel configs by default
TARGET_PIXEL_EXPERIENCE_2022 ?= true
ifneq ($(TARGET_DEVICE_IS_TABLET),true)
TARGET_PIXEL_EXPERIENCE_2023 ?= true
TARGET_PIXEL_EXPERIENCE_2024 ?= true
else
TARGET_PIXEL_TABLET_EXPERIENCE_2023 ?= true
endif

ifeq ($(TARGET_PIXEL_EXPERIENCE_2022),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2022.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2022.xml \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2022_midyear.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2022_midyear.xml \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2021.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2021.xml
endif

ifeq ($(TARGET_PIXEL_EXPERIENCE_2023),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2023.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2023.xml \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2023_midyear.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2023_midyear.xml
else ifeq ($(TARGET_PIXEL_TABLET_EXPERIENCE_2023),true)
PRODUCT_PACKAGES += \
    pixel_tablet_experience_2023.xml \
    pixel_tablet-initial-package-stopped-states.xml
endif

ifeq ($(TARGET_PIXEL_EXPERIENCE_2024),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2024.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2024.xml \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2024_midyear.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2024_midyear.xml
endif

$(call inherit-product, vendor/google/gms/common/common-vendor.mk)

# Properties
$(call inherit-product, vendor/google/gms/products/properties.mk)

# Inherit from apex config
$(call inherit-product, vendor/google/mainline_modules/config.mk)

# Pixel Framework
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/system_ext/etc/permissions/com.google.android.settings.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.google.android.settings.xml \
    vendor/google/gms/common/proprietary/system_ext/etc/permissions/com.google.android.systemui.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.google.android.systemui.xml

TARGET_INCLUDE_PIXEL_FRAMEWORKS ?= true
ifeq ($(TARGET_INCLUDE_PIXEL_FRAMEWORKS),true)
PRODUCT_PACKAGES += \
    SettingsGoogle \
    SystemUIGoogle

# Dex preopt
PRODUCT_DEXPREOPT_SPEED_APPS += \
    SettingsGoogle \
    SystemUIGoogle

else
PRODUCT_PACKAGES += \
    SettingsGoogleResources \
    SystemUIGoogleResources

endif

# Customizations
$(call inherit-product, vendor/google/gms/products/custom.mk)

# Face Unlock
ifeq ($(TARGET_FACE_UNLOCK_SUPPORTED),true)
PRODUCT_PACKAGES += \
    PixelTrafficLightFaceOverlay

PRODUCT_PACKAGES += \
    SettingsGoogleFutureFaceEnroll
endif

# CarrierSettings
ifeq ($(TARGET_INCLUDE_CARRIER_SETTINGS), true)

TARGET_REQUIRES_APN_CONF := false

# Include Carrier Runtime Configuration
PRODUCT_PACKAGES += \
    CarrierSettingsConfigOverlay \
    CarrierSettingsOverlay \
    CarrierSettingsProviderOverlay \
    CarrierSettingsSystemUIOverlay

# Include common SIM configuration proprieties
PRODUCT_VENDOR_PROPERTIES += \
    keyguard.no_require_sim=true \
    ro.com.android.prov_mobiledata=false
endif

ifeq ($(TARGET_INCLUDE_PIXEL_EUICC), true)
# Custom hidden api whitelist
PRODUCT_PACKAGES += \
    com.google.android.euicc_whitelist_custom.xml
endif

endif #WITH_GMS
