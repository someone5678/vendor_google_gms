# Overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += \
    vendor/google/gms/products/overlay

PRODUCT_PACKAGE_OVERLAYS += \
    vendor/google/gms/products/overlay/common

# GMS RRO overlay
PRODUCT_PACKAGES += \
    GmsConfigOverlayParasite \
    PixelConfigOverlayCustom \
    SettingsGoogleOverlayCustom \
    SystemUIGoogleOverlayCustom \
    DocumentsUIGoogleOverlayCustom \
    PixelLauncherOverlayCustom \
    PixelLauncherBlurOverlay

# Launcher overlay
ifeq ($(TARGET_DEVICE_IS_TABLET),true)
PRODUCT_PACKAGES += \
    NexusLauncherTabletOverlay
endif

ifeq ($(TARGET_SUPPORTS_CLEAR_CALLING),true)
PRODUCT_PACKAGES += \
    ClearCallingOverlay
endif

ifeq ($(TARGET_SUPPORTS_DREAMLINER),true)
PRODUCT_PACKAGES += \
    DreamlinerOverlay
endif

# Custom Google apps whitelist
PRODUCT_PACKAGES += \
    custom-google-framework-sysconfig.xml \
    custom-google-hiddenapi-package-whitelist.xml \
    custom-google-platform.xml \
    custom-google-power-whitelist.xml

# DSE search engine choice screen
ifeq ($(TARGET_SUPPORTS_DSE_CHOICE_SCREEN),true)
PRODUCT_PACKAGES += \
    dse_choice_screen.xml
endif

# EEA
ifeq ($(TARGET_EEA_V2_DEVICE),true)
PRODUCT_PACKAGES += \
    eea_v2_search_chrome.xml
else ifeq ($(TARGET_EEA_V1_DEVICE),true)
PRODUCT_PACKAGES += \
    eea_v1.xml
endif

# Custom GMS init script
PRODUCT_PACKAGES += \
    init.gms.rc

# Monet boot animation
PRODUCT_COPY_FILES += \
    vendor/google/gms/products/media/bootanimation-monet.zip:$(BOOT_ANIMATION_COPY_OUT)/media/bootanimation-monet.zip

# Themed icons for Pixel Launcher
$(call inherit-product, vendor/google/overlays/ThemeIcons/config.mk)

# Camera from Google (CameraGo)
ifeq ($(TARGET_INCLUDE_CAMERA_GO),true)
PRODUCT_PACKAGES += \
    CameraGo \
    privapp-permissions-camera-go

PRODUCT_PACKAGES += \
    PixelLauncherCustomOverlayCameraGo \
    PixelConfigOverlayCameraGo \
    SystemUICustomOverlayCameraGo
endif

# Enable Android (Go Edition) device specific features
ifeq ($(TARGET_SUPPORTS_LILY_EXPERIENCE),true)
PRODUCT_PACKAGES += \
    lily_experience
endif

ifeq ($(TARGET_INCLUDE_CARRIER_SERVICES), true)
PRODUCT_PACKAGES += \
    CarrierServices
endif
