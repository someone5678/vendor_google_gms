#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017-2020 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

set -e

DEVICE=common
VENDOR=google/gms

# Load extract_utils and do some sanity checks
MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "${MY_DIR}" ]]; then MY_DIR="${PWD}"; fi

ANDROID_ROOT="${MY_DIR}/../../.."

export TARGET_ENABLE_CHECKELF=true

HELPER="${ANDROID_ROOT}/tools/extract-utils/extract_utils.sh"
if [ ! -f "${HELPER}" ]; then
    echo "Unable to find helper script at ${HELPER}"
    exit 1
fi
source "${HELPER}"

# Initialize the helper
setup_vendor "${DEVICE}" "${VENDOR}" "${ANDROID_ROOT}" true

# Warning headers and guards
write_headers "arm64"
sed -i 's|TARGET_DEVICE|TARGET_ARCH|g' "${ANDROIDMK}"
sed -i 's|vendor/google/gms/|vendor/google/gms/common|g' "${PRODUCTMK}"
sed -i 's|device/google/gms//setup-makefiles.sh|vendor/google/gms/setup-makefiles.sh|g' "${ANDROIDBP}" "${ANDROIDMK}" "${BOARDMK}" "${PRODUCTMK}"
cat "${MY_DIR}/gmscore-mk.txt" >> "${ANDROIDMK}"
cat "${MY_DIR}/dexopt-bp.txt" >> "${ANDROIDBP}"

write_makefiles "${MY_DIR}/proprietary-files.txt" true

echo "" >> "${PRODUCTMK}"
echo "ifeq (\$(TARGET_INCLUDE_CARRIER_SETTINGS), true)" >> "${PRODUCTMK}"
write_makefiles "${MY_DIR}/proprietary-files-carriersettings.txt" true
echo "endif" >> "${PRODUCTMK}"

echo "" >> "${PRODUCTMK}"
echo "ifeq (\$(TARGET_INCLUDE_PIXEL_EUICC), true)" >> "${PRODUCTMK}"
write_makefiles "${MY_DIR}/proprietary-files-euicc.txt" true
echo "endif" >> "${PRODUCTMK}"

# Finish
write_footers

# Overlays
echo -e "\ninclude vendor/google/gms/common/overlays.mk" >> $PRODUCTMK

# Exclusions
for i in $(echo "\
    dreamliner Dreamliner \
    GCS SCONE \
    adaptivecharging TurboAdapter libpowerstatshaldataprovider \
    verizon Tycho VZWAPNLib AppDirectedSMSService \
    GoogleCamera com.google.android.camera \
    google-ril PlayAutoInstallConfig \
    Videos \
    YouTube \
    RecorderPrebuilt \
    arcore \
    PixelLiveWallpaperPrebuilt \
    pixel_experience_2021.xml pixel_experience_2022.xml \
    pixel_experience_2022_midyear.xml \
    pixel_experience_2023.xml pixel_experience_2023_midyear.xml \
    pixel_experience_2024.xml pixel_experience_2024_midyear.xml \
    battery_fail.png battery_scale.png \
    main_font.png animation.txt \
    com.google.android.settings.xml \
    com.google.android.systemui.xml \
    vendor_mac_permissions.xml \
    satellite_sos.xml \
    SatelliteGatewayPrebuilt \
    HealthIntelligencePrebuilt \
    firproximity.xml \
    SettingsGoogleFutureFaceEnroll \
    PixelAIPrebuilt \
    CreativeAssistant \
    bootanimation-dark.zip \
    bootanimation.zip \
"); do
    sed -i "/$i/d" "${ANDROID_ROOT}/vendor/google/gms/common/common-vendor.mk"
done

for i in $(echo "\
    PixelTrafficLightFaceOverlay \
    PearlOverlay2024 \
"); do
    sed -i "/$i/d" "${ANDROID_ROOT}/vendor/google/gms/common/overlays.mk"
done

sed -i 's|	name: "MeetPrebuilt",|	name: "MeetPrebuilt",\n	skip_preprocessed_apk_checks: true,|g' "${ANDROIDBP}"
