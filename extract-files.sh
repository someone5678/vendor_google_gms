#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017-2020 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

set -e

DEVICE=common
VENDOR=google/gms

# Load extract_utils and do some sanity checks
MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "${MY_DIR}" ]]; then MY_DIR="${PWD}"; fi

ANDROID_ROOT="${MY_DIR}/../../.."

export TARGET_ENABLE_CHECKELF=true

HELPER="${ANDROID_ROOT}/tools/extract-utils/extract_utils.sh"
if [ ! -f "${HELPER}" ]; then
    echo "Unable to find helper script at ${HELPER}"
    exit 1
fi
source "${HELPER}"

# Default to sanitizing the vendor folder before extraction
CLEAN_VENDOR=true

KANG=
SECTION=

while [ "${#}" -gt 0 ]; do
    case "${1}" in
        -n | --no-cleanup)
            CLEAN_VENDOR=false
            ;;
        -k | --kang)
            KANG="--kang"
            ;;
        -s | --section)
            SECTION="${2}"
            shift
            CLEAN_VENDOR=false
            ;;
        *)
            SRC="${1}"
            ;;
    esac
    shift
done

if [ -z "${SRC}" ]; then
    SRC="adb"
fi

function blob_fixup() {
    case "${1}" in
        product/priv-app/DevicePersonalizationPrebuiltPixel/DevicePersonalizationPrebuiltPixel.apk | \
        product/priv-app/GoogleCamera/GoogleCamera.apk | \
        product/priv-app/PrebuiltBugle/PrebuiltBugle.apk | \
        product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreVic.apk | \
        product/priv-app/Velvet/Velvet.apk | \
        product/app/Photos/Photos.apk | \
        product/app/PrebuiltGmail/PrebuiltGmail.apk | \
        product/priv-app/PixelLiveWallpaperPrebuilt/PixelLiveWallpaperPrebuilt.apk)
            [ "$2" = "" ] && return 0
            split --bytes=49M -d "$2" "$2".part
            ;;
        product/overlay/*apk)
            [ "$2" = "" ] && return 0
            starletMagic $1 $2 &
            ;;
        product/etc/permissions/privapp-permissions-google-p.xml)
            [ "$2" = "" ] && return 0
            sed -i '/com.google.android.googlequicksearchbox">/a\        <permission name="android.permission.RECORD_AUDIO"/>' $2 &
            ;;
        product/media/bootanimation-dark.zip | \
        product/media/bootanimation.zip)
            [ "$2" = "" ] && return 0
            folder=${2/.zip/}
            filename=$(basename $1)
            unzip $2 -d $folder
            rm -rf $2
            sed -i 's/960 2142 60/960 2142 60 1/g' $folder/desc.txt
            (cd $folder && zip -r -0 ../$filename *)
            rm -rf $folder
            ;;
        vendor/etc/selinux/vendor_mac_permissions.xml)
            [ "$2" = "" ] && return 0
            googlePlfSigUpdater $2
            ;;
        *)
            return 1
            ;;
    esac

    return 0
}

function blob_fixup_dry() {
    blob_fixup "$1" ""
}

function starletMagic() {
    folder=${2/.apk/}

    echo "    "${folder##*/} "\\" >> "${MY_DIR}/${DEVICE}/overlays.mk"

    apktool -q d "$2" -o $folder -f
    rm -rf $2 $folder/{apktool.yml,original,res/values/public.xml,unknown}

    cp ${MY_DIR}/overlay-template.txt $folder/Android.bp
    sed -i "s|dummy|${folder##*/}|g" $folder/Android.bp

    find $folder -type f -name AndroidManifest.xml \
        -exec sed -i "s|extractNativeLibs\=\"false\"|extractNativeLibs\=\"true\"|g" {} \;

    find $folder -type f -name AndroidManifest.xml \
        -exec sed -i "s|android:priority\=\"1\"|android:priority\=\"2\"|g" {} \;

    find $folder -type f -name AndroidManifest.xml \
        -exec sed -i "s|auto_generated_rro_product__|pixel.overlay|g" {} \;

    for file in $(find $folder/res -name *xml \
            ! -path "$folder/res/raw" \
            ! -path "$folder/res/drawable*" \
            ! -path "$folder/res/xml"); do
        for tag in $(cat exclude-tag.txt); do
            type=$(echo $tag | cut -d: -f1)
            node=$(echo $tag | cut -d: -f2)
            xmlstarlet ed -L -d "/resources/$type[@name="\'$node\'"]" $file
            xmlstarlet fo -s 4 $file > $file.bak
            mv $file.bak $file
        done
        sed -i "s|\?android:\^attr-private|\@\*android\:attr|g" $file
        sed -i "s|\@android\:color|\@\*android\:color|g" $file
        sed -i "s|\^attr-private|attr|g" $file
    done

    if [ "${folder##*/}" == "SettingsGoogleOverlay2021AndNewer" ]; then
        sed -i "s| android\:resourcesMap\=\"\@xml\/overlays\"||g" "$folder/AndroidManifest.xml"
        rm -rf "$folder/res/xml"
    fi

    if [[ "${folder##*/}" = "PixelSystemUIGoogleOverlay" ]]; then
        local allowed_pkg=(
            "com.android.systemui.falcon"
            "com.android.systemui.falcon.debug"
            "com.android.systemui.falcon.one"
            "com.android.systemui.falcon.two"
            "com.android.systemui.falcon.three"
            "com.android.systemui.falcon.four"
            "com.android.systemui.falcon.five"
            "com.android.systemui.falcon.six"
            "com.android.systemui.falcon.seven"
            "com.android.systemui.falcon.eight"
            "com.android.systemui.falcon.nine"
            "com.google.oslo"
            "org.lineageos.settings.device"
        )

        for pkg in ${allowed_pkg[@]}
        do
            xmlstarlet ed -L \
                -s "//resources/string-array[@name='config_pluginAllowlist']" \
                -t elem -n "item" -v $pkg \
                "$folder/res/values/arrays.xml"
        done
        xmlstarlet fo -s 4 "$folder/res/values/arrays.xml" > "$folder/res/values/arrays.xml".bak
        mv "$folder/res/values/arrays.xml".bak "$folder/res/values/arrays.xml"
    fi
}

function googlePlfSigUpdater() {
    google_signature_config="${MY_DIR}/products/rro_overlay/GmsConfigOverlayParasite/res/values/google_signature_config.xml"

    xmlstarlet ed -L \
        -d "/resources/string-array[@name='config_googlePlatformSignatures']/item" \
        $google_signature_config

    while IFS= read -r sigs; do
        xmlstarlet ed -L \
            -s "/resources/string-array[@name='config_googlePlatformSignatures']" \
            -t elem -n "item" -v $sigs \
            $google_signature_config
    done < <(xmlstarlet sel \
                -t -v "//policy/signer[seinfo/@value='platform']/@signature" -n \
                "$1")

    xmlstarlet fo -s 4 $google_signature_config > $google_signature_config.bak
    mv $google_signature_config.bak $google_signature_config

    rm $1
}

# Initialize the helper
setup_vendor "${DEVICE}" "${VENDOR}" "${ANDROID_ROOT}" false "${CLEAN_VENDOR}"

echo "PRODUCT_PACKAGES += \\" > "${MY_DIR}/${DEVICE}/overlays.mk"

extract "${MY_DIR}/proprietary-files.txt" "${SRC}" "${KANG}" --section "${SECTION}"
extract "${MY_DIR}/proprietary-files-gmscore.txt" "${SRC}" "${KANG}" --section "${SECTION}"
extract "${MY_DIR}/proprietary-files-dexopt.txt" "${SRC}" "${KANG}" --section "${SECTION}"
extract "${MY_DIR}/proprietary-files-euicc.txt" "${SRC}" "${KANG}" --section "${SECTION}"
extract "${MY_DIR}/proprietary-files-carriersettings.txt" "${SRC}" "${KANG}" --section "${SECTION}"

"${MY_DIR}/setup-makefiles.sh"

echo "Waiting for extraction"
wait
echo "All done"
