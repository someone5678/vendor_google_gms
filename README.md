# vendor_google_gms

## Basic usage

- If you want to use this repository:
  1. `git clone https://gitlab.com/someone5678/vendor_google_gms-common-proprietary vendor/google/gms-common-proprietary`
  2. `$(call inherit-product-if-exists, vendor/google/gms/products/gms.mk)` to makefile.

## Advanced usage

### Initial setup
- Install `xmlstarlet`
  - `sudo apt install xmlstarlet`, `sudo pacman -S xmlstarlet`, etc. depending on your distribution.
- Install `apktool`, make sure its available in PATH
  - https://ibotpeaches.github.io/Apktool/install/
  - Run `apktool if framework-res.apk`, `apktool if SystemUIGoogle.apk` to install framework for apktool.
    <br>This is necessary for proper extracting.
    <br>(generally found in /system/framework/, /system_ext/priv-app/)

### Extracting / updating extracted overlays

Basically the same as regular extract-files

```sh
./extract-files.sh /path/to/a/dump
```

### Removing certain overlays

- If certain overlays are not required, you can simply add them to `exclude-tag.txt`, those will be removed from the extracted overlays.

### Miscellaneous information

- The main changes here are the addition of `apktool` and `xmlstarlet`, to allow us to extract the necessary overlays from some RROs.
- Since the process does take a while, to speed it up, all extraction is done in the background.
  <br>This might not be suitable for weaker systems if there is a very high number of overlays to be extracted.

## Variables for including GMS

Common:
* `TARGET_SUPPORTS_GOOGLE_RECORDER`: Include Google Recorder app to build.
* `TARGET_INCLUDE_STOCK_ARCORE`: Include Pixel AR Core app to build.
* `TARGET_INCLUDE_LIVE_WALLPAPERS`: Include Pixel Live Wallpaper app to build.
* `TARGET_SUPPORTS_QUICK_TAP`: Include required permission for back tap gestures.<br>
  Should check whether device supports it.
* `TARGET_SUPPORTS_CALL_RECORDING`: Enable Call Recording support in Google Phone app.
* `TARGET_CHARGER_RESOURCE_COPY_OUT`: Determine where to copy offline charging animation resources.<br>
  (e.g. `TARGET_CHARGER_RESOURCE_COPY_OUT := vendor`)
* `TARGET_BOOT_ANIMATION_COPY_OUT`: Determine where to copy bootanimation.<br>
  (e.g. `TARGET_BOOT_ANIMATION_COPY_OUT := system`)
* `TARGET_INCLUDE_PIXEL_FRAMEWORKS`: Include Pixel Framework library.
* `TARGET_INCLUDE_CARRIER_SETTINGS` : Include Carrier Settings to build.
* `TARGET_GBOARD_KEY_HEIGHT`: Resize GBoard ime key height to `TARGET_GBOARD_KEY_HEIGHT` (Must be float. e.g. 1.2)
* `TARGET_INCLUDE_PIXEL_EUICC`: Include Pixel eUICC to build.
* `TARGET_INCLUDE_CARRIER_SERVICES`: Include Google Carrier Services to build.
* `TARGET_INCLUDE_CAMERA_GO`: Include Camera from Google (Formerly, Camera Go or GCam Go) to build.
* `TARGET_SUPPORTS_LILY_EXPERIENCE`: Enabling Android (Go Edition) device specific features.
* `TARGET_SUPPORTS_GOOGLE_BATTERY := false`: Build TurboAdapter with dummy GoogleBatteryService.

Pixel Exclusive:
* `TARGET_SUPPORTS_GOOGLE_BATTERY := true`: Include required permissions and apps for Google Battery to build.<br>
  Your device must supports Google Battery.
* `TARGET_SUPPORTS_ADPATIVE_CHARGING`: Include required permissions for adaptive charging based on Google Battery.<br>
  Your device must supports Google Battery and adaptive charging.
* `TARGET_SUPPORTS_GOOGLE_CAMERA`: Include Google Camera app to build.
* `TARGET_GOOGLE_CAMERA_LARGE_RAM`: Include permission that indicates device has large ram for Google Camera.
* `TARGET_SUPPORTS_DREAMLINER`: Include required permissions and apps for Pixel Wireless Charging.<br>
  Your device must supports Google Battery and wireless charging.
* `TARGET_SUPPORTS_SATELLITE_SOS`: Include required permissions for Satellite SOS.<br>
  Your device must supports Satellite SOS.
* `TARGET_PIXEL_EXPERIENCE_2022`: Include permission that indicates device supports 2022 Google Tensor chipset related features.
* `TARGET_PIXEL_EXPERIENCE_2023`: Include permission that indicates device supports 2023 Google Tensor chipset related features.
* `TARGET_PIXEL_TABLET_EXPERIENCE_2023`: Include permission that indicates tablet device supports 2023 Google Tensor chipset related features.
* `TARGET_PIXEL_EXPERIENCE_2024`: Include permission that indicates device supports 2024 Google Tensor chipset related features.
* `TARGET_SUPPORTS_CLEAR_CALLING`: Include required overlay to enable clear calling feature support.
* `TARGET_SUPPORTS_FIR_PROXIMITY_SENSORS`: Include required packages to enable FIR proximity sensors support.
* `TARGET_SUPPORTS_PIXEL_SCREENSHOTS`: Include required packages to enable AI powered Pixel Screenshots support.
* `TARGET_SUPPORTS_PIXEL_STUDIO`: Include required packages to enable AI powered Pixel Studio support.
